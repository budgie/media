const AWS = require('aws-sdk')

const { Pool } = require('pg')

require('dotenv').config()
require('pg').defaults.ssl = true

const pool = new Pool()

AWS.config.update({
    endpoint: process.env.SCW_ENDPOINT,
    credentials: new AWS.Credentials({
        accessKeyId: process.env.SCW_ACCESS_KEY_ID,
        secretAccessKey: process.env.SCW_SECRET_ACCESS_KEY
    })
});

const s3 = new AWS.S3()

function uploadFile(file) {
    return new Promise(function (resolve, reject) {
        s3.upload({
                Bucket: process.env.SCW_BUCKET,
                Body: file.buffer, 
                Key: file.originalname,
                ACL:'public-read'    
            }, 
            (err, data) => {
                if (err) {
                    console.log(err)
                    reject(err)
                } else {
                    resolve(data)
                }
            }
        )
    })
}

function saveFile(idUser, idPost, path, isProfilPic) {
    return new Promise(function (resolve, reject) {
        
        pool.connect()
            .catch(e => reject(e))
            .then(r => { 
                if (isProfilPic) {
                    pool.query(
                        `UPDATE files
                        SET isProfilPic = FALSE
                        WHERE idUser = $1`,
                        [idUser]
                    )
                    .then(function (res) {
                        pool.query(
                            `INSERT INTO files (id, idUser, path, isProfilPic)
                            VALUES (uuid_generate_v1(), $1, $2, TRUE)`,
                            [idUser, path],
                            (err, res) => {
                                if (err) {
                                    reject(err)
                                } else {
                                    resolve()
                                }
                            }
                        )
                    })
                    .catch(function (err) {
                        reject(err)
                    })
                } else {
                    pool.query(
                        `INSERT INTO files (id, idUser, idPost, path, isProfilPic)
                        VALUES (uuid_generate_v1(), $1, $2, $3, $4)`,
                        [idUser, idPost, path, isProfilPic],
                        (err, res) => {
                            
                            if (err) {
                                reject(err)
                            } else {
                                resolve()
                            }
                        }
                    )
                }
            }
        )
    })
}

module.exports = { uploadFile, saveFile }
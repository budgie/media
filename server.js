const express = require('express')
const multer = require('multer')
const bodyParser = require('body-parser')
const cors = require("cors");

const { Pool } = require('pg')

const {
    uploadFile,
    saveFile
} = require('./file')

require('dotenv').config()
require('pg').defaults.ssl = true

const pool = new Pool();

const app = express()

app.use(bodyParser.json())
app.use(cors());

var upload = multer({storage: multer.memoryStorage()})

// Permet d'ajouter un fichier lié à un utilisateur qui n'est pas sa photo de profil (utiliser '/files/profilPicture' sinon)
app.post('/files', upload.single('file'), function(req, res) {
    const idUser = req.body.idUser
    const idPost = req.body.idPost

    uploadFile(req.file)
        .then(function (result) {
            saveFile(idUser, idPost, result.Location, false)
                .then(function (result) {
                    res.status(200).send(result)
                })
                .catch(function (err) {
                    console.log(err)
                    res.status(500).send(err)
                })
        })
        .catch(function (err) {
            res.status(500).send(err)
        })
})

// Permet d'ajouter un fichier et de l'enregistrer en temps que photo de profil
app.post('/files/profilPicture', upload.single('file'), function (res, res) {
    const idUser = req.body.idUser

    uploadFile(req.file)
        .then(function (result) {
            saveFile(idUser, result.Location, true)
                .then(function (result) {
                    res.status(200).send(result)
                })
                .catch(function (err) {
                    res.status(500).send(err)
                })
        })
        .catch(function (err) {
            res.status(500).send(err)
        })
})

// Recuperer la photo de profil d'un utilisateur
app.get('/files/users/:idUser/profilPicture', function (req, res) {
    const idUser = req.params.idUser

    pool.connect()
        .catch(e => {console.log(e);res.status(500).send(e)})
        .then(r => {                
            pool.query(
                `SELECT path
                FROM files 
                WHERE idUser = $1 
                    AND isProfilPic = TRUE`,
                [idUser],
                (err, result) => {
                    if (err) {
                        res.status(500).send(err)
                    } else {
                        res.status(200).json({path: result.rows === undefined ? null : result.rows[0].path})
                    }
                }
            )
        }
    )
})

// Recuperer les photos d'un post
app.get('/files/posts/:idPost', async function (req, res) {
    const idPost = req.params.idPost

    const client = await pool.connect()
    client.query(
        {
            text: `SELECT path FROM files WHERE idpost = $1`,
            values: [idPost]
        },
        (err, result) => {
            client.release()
            if (err) {
                console.log(err)
                res.status(500).json(err)
            } else {
                res.status(200).json({media: (result.rows === undefined ? [] : result.rows)})
            }
        }
    )
})

// Recuperer les photos d'un utilisateur
app.get('/files/users/:idUser', function (req, res) {
    const idUser = req.params.idUser

    pool.connect()
        .catch(e => {console.log(e);res.status(500).send(e)})
        .then(r => {                
            pool.query(
                `SELECT path
                FROM files 
                WHERE idUser = $1`,
                [idUser],
                (err, result) => {
                    if (err) {
                        res.status(500).send(err)
                    } else {
                        res.status(200).json({path: result.rows === undefined ? [] : result.rows})
                    }
                }
            )
        }
    )
})

app.listen(process.env.PORT, process.env.IP, function () {
    console.log(`Listening on ${process.env.IP}:${process.env.PORT}`)
})
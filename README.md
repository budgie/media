# Media

The back Media manages 


### Prerequisites

You need to install [npm](https://www.npmjs.com/get-npm) and [nodejs](https://nodejs.org/en/download/).

### Installing

Go in the project then install dependencies

```
npm install
```

You need to put the following variables in a .env file based on variables needed in object storage on Scaleway
```
SCW_ACCESS_KEY_ID=<SCW_ACCESS_KEY_ID>
SCW_SECRET_ACCESS_KEY=<SCW_SECRET_ACCESS_KEY>
SCW_ENDPOINT=<SCW_ENDPOINT>
SCW_BUCKET=<SCW_BUCKET>
PGUSER=<User>
PGHOST=<Host>
PGPASSWORD=<Password>
PGDATABASE=<DBName>
PGPORT=<DBPort>
IP=localhost
PORT=<Port>
PGSSLMODE=require
```

### Launch

You just have to do the following command

```
node server.js
```
